# 内置组件

内置组件自simplepro 2.0+ 可用，低于2.0 不可用。

以下字段均在demo中有提供例子参考，[demo下载](https://github.com/newpanjing/simplepro_demo)

> 外键字段、多对多字段等搜索问题,请参考[外键字段搜索](/field_search.md)

> 友情提示：由于django机制问题，simplepro的组件在内联(inline)表单中无法使用，后续我们会继续尝试其他办法来实现。

## 组件列表
|组件名|组件说明|版本|
|:--|:--|:--|
|[`CheckBox`](#CheckBox-复选框)|复选框|>=2.0|
|[`Radio`](#radio-单选框)|单选框|>=2.0|
|[`Switch`](#switch)|布尔类型切换框|>=2.0|
|[`InputNumber`](#inputnumber)|带加减的数字输入框|>=2.0|
|[`Slider`](#slider滑块)|滑块|>=2.0|
|[`Image`](#image图片上传)|滑块|>=2.0|
|[`IntegerField`](#integerfield-int字段)|int字段|>=2.0|
|[`Rate`](#rate-评分)|评分|>=2.0|
|[`Time`](#time-时间选择器)|时间选择器|>=2.0|
|[`Date`](#date-日期选择器)|日期选择器|>=2.0|
|[`DateTime`](#datetime-日期时间选择器)|日期时间选择器|>=2.0|
|[`Char`](#char文本输入框)|文本输入框|>=2.0|
|[`Foreignkey`](#foreignkey-外键字段-select)|外键字段-select|>=2.0|
|[`OneToOneField`](#onetoonefield-外键字段-select)|一对一字段|>=2.0|
|[`ManyToManyField`](#manytomanyfield-多对多字段-多选-select)|多对多字段|>=2.0|
|[`Transfer`](#transfer-穿梭框)|穿梭框|>=2.0|
|[`Layer`](#layer对话框按钮)|穿梭layer对话框按钮框|>=2.1|
|[`AMap`](#高德地图)|高德地图组件|>=3.1|
|[`Video`](#视频播放器)|视频播放器|>=5.0|
|[`TreeCombobox`](#树形下拉框)|树形下拉框|>=6.0|


## CheckBox 复选框

复选框组件，一组备选项中进行多选

[https://element.eleme.cn/#/zh-CN/component/checkbox](https://element.eleme.cn/#/zh-CN/component/checkbox)


### 效果

<img :src="$withBase('/images/checkbox.png')" width='100%'>

### 字段

+ 类型

继承自`model.CharField`字段

+ 包

`simplepro.components.fields.CheckboxField`


### 参数

除了下列参数以外，其他参数与`model.CharField`一致。

|参数名|类型|必选|说明|
|---|---|---|---|
|choices|元组或数组|是|为checkbox指定选项|

### 例子

```python

from django.db import models
from simplepro.components import fields

class CheckBoxModelTest(models.Model):
    type_choices = (
        (0, '选项1'),
        (1, '选项2'),
        (2, '选项3'),
        (3, '选项4'),
    )

    # 必须包含 choices 字段，否则报错

    f = fields.CheckboxField(choices=type_choices, verbose_name='复选框', default=0, help_text='继承自CharField，逗号分隔',
                             max_length=128)

    class Meta:
        verbose_name = 'Checkbox复选框'
        verbose_name_plural = 'Checkbox复选框'
```



## Radio 单选框

Radio 单选框，在一组备选项中进行单选

[https://element.eleme.cn/#/zh-CN/component/radio](https://element.eleme.cn/#/zh-CN/component/radio)

### 效果

<img :src="$withBase('/images/radio.png')" width='100%'>

### 字段


+ 类型

继承自`model.IntegerField`字段

+ 包

`simplepro.components.fields.RadioField`

### 参数

除了下列参数以外，其他参数与`model.IntegerField`一致。

|参数名|类型|必选|说明|
|---|---|---|---|
|choices|元组或数组|是|为checkbox指定选项|

### 例子

```python

from django.db import models
from simplepro.components import fields

class RadioModel(models.Model):
    type_choices = (
        (0, '选项1'),
        (1, '选项2'),
        (2, '选项3'),
        (3, '选项4'),
    )

    # 必须包含 choices 字段，否则报错
    f = fields.RadioField(choices=type_choices, verbose_name='单选框', default=0, help_text='继承自IntegerField')

    class Meta:
        verbose_name = 'Radio单选框'
        verbose_name_plural = 'Radio单选框'
```


## Switch

Switch 开关，表示两种相互对立的状态间的切换，多用于触发「开/关」。

[https://element.eleme.cn/#/zh-CN/component/switch](https://element.eleme.cn/#/zh-CN/component/switch)

### 效果

<img :src="$withBase('/images/switch.png')" width='100%'>

### 字段


+ 类型

继承自`model.BooleanField`字段

+ 包

`simplepro.components.fields.SwitchField`

### 参数

参数与`model.BooleanField`一致，无特有参数。


### 例子

```python

from django.db import models
from simplepro.components import fields

class SwitchModel(models.Model):
    f = fields.SwitchField(default=False, verbose_name='复选框', help_text='继承自BooleanField')

    class Meta:
        verbose_name = 'Switch切换'
        verbose_name_plural = 'Switch切换'
```


## InputNumber

InputNumber 计数器，仅允许输入标准的数字值，可定义范围

[https://element.eleme.cn/#/zh-CN/component/input-number](https://element.eleme.cn/#/zh-CN/component/input-number)

### 效果

<img :src="$withBase('/images/input_number.png')" width='100%'>

### 字段


+ 类型

继承自`model.IntegerField`字段

+ 包

`simplepro.components.fields.InputNumberField`

### 参数

除以下参数外其他参数与`model.IntegerField`一致

|参数名|类型|必选|说明|
|---|---|---|---|
|min_value|Integer|否|最小值|
|max_value|Integer|否|最大值|

### 例子

```python

from django.db import models
from simplepro.components import fields

class InputNumberModel(models.Model):
    f = fields.InputNumberField(max_value=100, min_value=1, default=1, verbose_name='InputNumber计数器',
                                help_text='继承自IntegerField')

    class Meta:
        verbose_name = 'InputNumber计数器'
        verbose_name_plural = 'InputNumber计数器'
```


## Slider滑块

Slider 滑块，通过拖动滑块在一个固定区间内进行选择

[https://element.eleme.cn/#/zh-CN/component/slider](https://element.eleme.cn/#/zh-CN/component/slider)

### 效果

<img :src="$withBase('/images/slider.png')" width='100%'>

### 字段


+ 类型

继承自`model.IntegerField`字段

+ 包

`simplepro.components.fields.SliderField`

### 参数

除了下列参数以外，其他参数与`model.IntegerField`一致。

|参数名|类型|必选|说明|
|---|---|---|---|
|min_value|int|否|最小值|
|max_value|int|否|最大值|
|input_size|string|否|输入框的尺寸，large / medium / small / mini，默认：small|
|step|int|否|步长，默认：1|
|show_tooltip|boolean|否|是否显示 tooltip，默认：True|
|vertical|boolean|否|是否竖向模式，默认：False|
|height|string|否|Slider竖向模式时高度，默认：100px|
|width|string|否|Slider横向模式时宽度，默认：200px|
|show_input|boolean|否|是否显示输入框，默认：False|



### 例子

```python

from django.db import models
from simplepro.components import fields


class SliderModel(models.Model):
    # input_size=large / medium / small / mini
    # show-tooltip
    # vertical=False
    f1 = fields.SliderField(show_input=True, max_value=100, min_value=1, step=1, input_size='large', show_tooltip=True,
                            default=1,
                            verbose_name='Slider滑块',
                            help_text='继承自IntegerField')

    f2 = fields.SliderField(max_value=1000,
                            min_value=1,
                            step=10,
                            input_size='mini',
                            width='50%',
                            default=1,
                            show_tooltip=False,
                            verbose_name='Slider滑块',
                            help_text='继承自IntegerField')

    f3 = fields.SliderField(max_value=100,
                            min_value=1,
                            step=2,
                            input_size='medium',
                            vertical=True,
                            height='100px',
                            default=1, verbose_name='Slider滑块',
                            help_text='继承自IntegerField')

    class Meta:
        verbose_name = 'Slider滑块'
        verbose_name_plural = 'Slider滑块'
```



## Image图片上传

通过点击或者拖拽上传文件

[https://element.eleme.cn/#/zh-CN/component/upload](https://element.eleme.cn/#/zh-CN/component/upload)

### 效果

<img :src="$withBase('/images/image.png')" width='100%'>

### 字段


+ 类型

继承自`model.CharField`字段

+ 包

`simplepro.components.fields.ImageField`

### 参数

除了下列参数以外，其他参数与`model.CharField`一致。

|参数名|类型|必选|说明|
|---|---|---|---|
|drag|boolean|否|是否启用拖拽上传图片|
|action|string|否|指定图片上传的接口|
|accept|string|否|自版本3.4+支持，限制图片的格式，默认为：`.png,.jpg,.jpeg,.gif,.bmp,.webp,.psd,.icns,.icon,.heic,.heif,.tiff,.tif`，文件类型与原生的`file`组件`accept`相同，[accept文档](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input#attr-accept)|

#### action 说明

配置action后，选择图片后将向该接口上传图片，然后需要返回json串

成功：

```json
{"success": 1, "message": "上传成功！", "url": "/meida//rbwTbhOR.png"}
```

失败：

```json
{"success": 0, "message": "上传失败！"}
```

### 例子

```python

from django.db import models
from simplepro.components import fields

class ImageModel(models.Model):
    # drag 是否可拖拽上传文件
    f1 = fields.ImageField(drag=True, verbose_name='图片上传', max_length=128)

    f2 = fields.ImageField(drag=False,
                           action='/123',  # 可以手动指定一个上传的url地址
                           verbose_name='图片上传', max_length=128)
    # 限制只能传png图片，多个用逗号分隔，例如：.png,.jpg,.jpeg
    f3= fields.ImageField(drag=False,
                          accept=".png",
                          verbose_name='图片上传', max_length=128)
    class Meta:
        verbose_name = 'Image图片上传'
        verbose_name_plural = 'Image图片上传'
```

## IntegerField int字段

基础类型 int字段，如果有`choices`属性就会渲染成`Select` 没有就渲染成普通的输入框

### 效果

<img :src="$withBase('/images/integerfield.png')" width='100%'>

### 字段
    
+ 类型

继承自`model.IntegerField`字段

+ 包

`simplepro.components.fields.IntegerField`

### 参数

除了下列参数以外，其他参数与`model.IntegerField`一致。

|参数名|类型|必选|说明|
|---|---|---|---|
|clearable|boolean|否|是否可以清空选项|
|placeholder|string|否|占位符|
|filterable|boolean|否|列表过滤搜索|


### 例子

```python

from django.db import models
from simplepro.components import fields

class IntegerModel(models.Model)
    school_choices = (
            (0, '北大'),
            (1, '清华'),
            (2, '复旦'),
            (3, '交大'),
            (4, '厦大'),
            (5, '深大'),
            (6, '中山大学'),
            (7, '东南大学'),
            (8, '南开大学'),
        )
    school = fields.IntegerField(verbose_name='学校', choices=school_choices, default=0)

```

## Rate 评分

评分组件

[https://element.eleme.cn/2.13/#/zh-CN/component/rate](https://element.eleme.cn/2.13/#/zh-CN/component/rate)

### 效果

<img :src="$withBase('/images/rate.png')" width='100%'>

### 字段


+ 类型

继承自`model.FloatField`字段

+ 包

`simplepro.components.fields.RateField`

### 参数

除了下列参数以外，其他参数与`model.FloatField`一致。

|参数名|类型|必选|说明|
|---|---|---|---|
|max_value|int|否|评论组件最大星级，默认：5|
|allow_half|boolean|否|是否允许半选，默认：False|
|disabled|boolean|否|是否为只读，默认：False|
|show_score|boolean|否|是否显示当前分数，默认：True|

### 例子

```python

from django.db import models
from simplepro.components import fields


class RateModel(models.Model):
    f1 = fields.RateField(verbose_name='评分1', max_value=5)

    # 指定最大值，和允许选半格
    f2 = fields.RateField(verbose_name='评分2', max_value=5, allow_half=True, show_score=False)

    # disabled 设为默认读
    f3 = fields.RateField(verbose_name='评分3', max_value=5, default=3.5, disabled=True)

    class Meta:
        verbose_name = 'Rate评分'
        verbose_name_plural = 'Rate评分'

```

## Time 时间选择器

用于选择或输入时间，小时和分

至于为什么不支持秒，是因为django本身不支持。

[https://element.eleme.cn/2.13/#/zh-CN/component/time-picker](https://element.eleme.cn/2.13/#/zh-CN/component/time-picker)

### 效果

<img :src="$withBase('/images/time.png')" width='100%'>

### 字段
    
+ 类型

继承自`model.TimeField`字段

+ 包

`simplepro.components.fields.TimeField`


### 参数

除了下列参数以外，其他参数与`model.TimeField`一致。

|参数名|类型|必选|说明|
|---|---|---|---|
|prefix_icon|string|否|输入框靠左图标，默认：el-icon-date|
|clear_icon|string|否|清除按钮图标，默认：el-icon-circle-close|
|align|string|否|对齐方式，默认：left，可选：left / center / right|
|size|string|否|输入框尺寸，可选：	medium / small / mini|
|clearable|boolean|否|是否显示清除按钮，默认：True|
|editable|boolean|否|是否文本框可输入，默认：True|
|disabled|boolean|否|禁用，默认：False|
|readonly|boolean|否|完全只读，默认：False|

### 例子

```python

from django.db import models
from simplepro.components import fields

class TimeModel(models.Model):
    f1 = fields.TimeField(verbose_name='Time时间选择1')

    f2 = fields.TimeField(verbose_name='Time时间选择2', default=timezone.now, clearable=False, help_text='不可清除')

    f3 = fields.TimeField(verbose_name='Time时间选择3', default=timezone.now,
                          align='right', clearable=False, editable=False, readonly=True, help_text='不可编辑')

    class Meta:
        verbose_name = 'Time时间选择'
        verbose_name_plural = 'Time时间选择'

```

## Date 日期选择器

用于选择或输入日期，年月日

[https://element.eleme.cn/2.13/#/zh-CN/component/date-picker](https://element.eleme.cn/2.13/#/zh-CN/component/date-picker)

### 效果

<img :src="$withBase('/images/date.png')" width='100%'>

### 字段
    
+ 类型

继承自`model.DateField`字段

+ 包

`simplepro.components.fields.DateField`


### 参数

除了下列参数以外，其他参数与`model.DateField`一致。

|参数名|类型|必选|说明|
|---|---|---|---|
|prefix_icon|string|否|输入框靠左图标，默认：el-icon-date|
|clear_icon|string|否|清除按钮图标，默认：el-icon-circle-close|
|align|string|否|对齐方式，默认：left，可选：left / center / right|
|size|string|否|输入框尺寸，可选：	medium / small / mini|
|clearable|boolean|否|是否显示清除按钮，默认：True|
|editable|boolean|否|是否文本框可输入，默认：True|
|disabled|boolean|否|禁用，默认：False|
|readonly|boolean|否|完全只读，默认：False|
|options|string/dict|配置，可以是json字符串，也可以是dict，[配置详情](#options字段说明)|

#### options字段说明

`options`字段 实际上对应的就是 elementui time、date、datetime这三个的`picker-options`属性。

例如给选择器增加快捷操作：

```python
options1={
          disabledDate(time) {
            return time.getTime() > Date.now();
          },
          shortcuts: [{
            text: '今天',
            onClick(picker) {
              picker.$emit('pick', new Date());
            }
          }, {
            text: '昨天',
            onClick(picker) {
              const date = new Date();
              date.setTime(date.getTime() - 3600 * 1000 * 24);
              picker.$emit('pick', date);
            }
          }, {
            text: '一周前',
            onClick(picker) {
              const date = new Date();
              date.setTime(date.getTime() - 3600 * 1000 * 24 * 7);
              picker.$emit('pick', date);
            }
          }]
        }
f1 = fields.DateField(verbose_name='Date日期选择1', options=options1)
```

### 例子

```python

from django.db import models
from simplepro.components import fields

class DateModel(models.Model):
    # options1 可以是个dict 也可以是个str，
    # 但是最终 是要一个完整的json串，
    # 否则可能导致报错控件无法显示出来

    options1 = """
    {
          disabledDate(time) {
            return time.getTime() > Date.now();
          },
          shortcuts: [{
            text: '今天',
            onClick(picker) {
              picker.$emit('pick', new Date());
            }
          }, {
            text: '昨天',
            onClick(picker) {
              const date = new Date();
              date.setTime(date.getTime() - 3600 * 1000 * 24);
              picker.$emit('pick', date);
            }
          }, {
            text: '一周前',
            onClick(picker) {
              const date = new Date();
              date.setTime(date.getTime() - 3600 * 1000 * 24 * 7);
              picker.$emit('pick', date);
            }
          }]
        }
    """

    # 设置快捷选项
    f1 = fields.DateField(verbose_name='Date日期选择1', options=options1)

    f2 = fields.DateField(verbose_name='Date日期选择2', default=timezone.now, clearable=False, help_text='不可清除')

    f3 = fields.DateField(verbose_name='Date日期选择3', default=timezone.now,
                          align='right', clearable=False, editable=False, readonly=True, help_text='不可编辑')

    class Meta:
        verbose_name = 'Date日期选择'
        verbose_name_plural = 'Date日期选择'

```

## DateTime 日期时间选择器

用于选择或输入日期时间，年-月-日 时:分:秒

[https://element.eleme.cn/2.13/#/zh-CN/component/datetime-picker](https://element.eleme.cn/2.13/#/zh-CN/component/datetime-picker)

### 效果

<img :src="$withBase('/images/datetime.png')" width='100%'>

### 字段
    
+ 类型

继承自`model.DateTimeField`字段

+ 包

`simplepro.components.fields.DateTimeField`


### 参数

除了下列参数以外，其他参数与`model.DateTimeField`一致。

|参数名|类型|必选|说明|
|---|---|---|---|
|prefix_icon|string|否|输入框靠左图标，默认：el-icon-date|
|clear_icon|string|否|清除按钮图标，默认：el-icon-circle-close|
|align|string|否|对齐方式，默认：left，可选：left / center / right|
|size|string|否|输入框尺寸，可选：	medium / small / mini|
|clearable|boolean|否|是否显示清除按钮，默认：True|
|editable|boolean|否|是否文本框可输入，默认：True|
|disabled|boolean|否|禁用，默认：False|
|readonly|boolean|否|完全只读，默认：False|
|options|string/dict|配置，可以是json字符串，也可以是dict，[配置详情](#datetime_options字段说明)|

#### datetime_options字段说明

`options`字段 实际上对应的就是 elementui time、date、datetime这三个的`picker-options`属性。

例如给选择器增加快捷操作：

```python
options1={
          disabledDate(time) {
            return time.getTime() > Date.now();
          },
          shortcuts: [{
            text: '今天',
            onClick(picker) {
              picker.$emit('pick', new Date());
            }
          }, {
            text: '昨天',
            onClick(picker) {
              const date = new Date();
              date.setTime(date.getTime() - 3600 * 1000 * 24);
              picker.$emit('pick', date);
            }
          }, {
            text: '一周前',
            onClick(picker) {
              const date = new Date();
              date.setTime(date.getTime() - 3600 * 1000 * 24 * 7);
              picker.$emit('pick', date);
            }
          }]
        }
f1 = fields.DateField(verbose_name='Date日期选择1', options=options1)
```

### 例子

```python

from django.db import models
from simplepro.components import fields


class DateTimeModel(models.Model):
    # 可以设置 快捷操作
    # options1 可以是个dict 也可以是个str，
    # 但是最终 是要一个完整的json串，
    # 否则可能导致报错控件无法显示出来
    # 文档地址：https://element.eleme.cn/2.13/#/zh-CN/component/datetime-picker
    options1 = """
    {
          shortcuts: [{
            text: '今天',
            onClick(picker) {
              picker.$emit('pick', new Date());
            }
          }, {
            text: '昨天',
            onClick(picker) {
              const date = new Date();
              date.setTime(date.getTime() - 3600 * 1000 * 24);
              picker.$emit('pick', date);
            }
          }, {
            text: '一周前',
            onClick(picker) {
              const date = new Date();
              date.setTime(date.getTime() - 3600 * 1000 * 24 * 7);
              picker.$emit('pick', date);
            }
          }]
        }
    """
    f1 = fields.DateTimeField(verbose_name='DateTime日期时间1', options=options1)

    f2 = fields.DateTimeField(verbose_name='DateTime日期时间2', default=timezone.now, clearable=False, help_text='不可清除')

    f3 = fields.DateTimeField(verbose_name='DateTime日期时间3', default=timezone.now,
                              align='right', clearable=False, editable=False, readonly=True, help_text='不可编辑')

    class Meta:
        verbose_name = 'DateTime日期时间'
        verbose_name_plural = 'DateTime日期时间'


```

## Char文本输入框

Input 输入框，通过鼠标或键盘输入字符

[https://element.eleme.cn/2.13/#/zh-CN/component/input](https://element.eleme.cn/2.13/#/zh-CN/component/input)

### 效果

<img :src="$withBase('/images/char.png')" width='100%'>

### 字段
    
+ 类型

继承自`model.CharField`字段

+ 包

`simplepro.components.fields.CharField`

### 参数

除了下列参数以外，其他参数与`model.CharField`一致。

|参数名|类型|必选|说明|
|---|---|---|---|
|input_type|string|否|类型，text，textarea 和其他 [原生 input 的 type 值](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input#Form_%3Cinput%3E_types)|
|max_length|int|否|原生属性，最大输入长度|
|min_length|int|否|原生属性，最小输入长度|
|placeholder|string|否|输入框占位文本|
|clearable|boolean|否|是否可清空|
|show_password|boolean|否|是否显示切换密码图标|
|disabled|boolean|否|禁用|
|size|string|否|输入框尺寸，只在 type!="textarea" 时有效，	medium / small / mini|
|prefix_icon|string|否|输入框头部[图标](https://element.eleme.cn/2.13/#/zh-CN/component/icon)|
|suffix_icon|string|否|输入框尾部[图标](https://element.eleme.cn/2.13/#/zh-CN/component/icon)|
|rows|int|否|输入框行数，只对 type="textarea" 有效|
|autocomplete|boolean|否|原生属性，自动补全|
|readonly|boolean|否|原生属性，是否只读|
|max_value|int|否|原生属性，设置最大值|
|min_value|int|否|原生属性，设置最小值|
|step|-|否|原生属性，设置输入字段的合法数字间隔|
|resize|string|否|控制是否能被用户缩放，取值：	none, both, horizontal, vertical|
|autofocus|boolean|否|原生属性，自动获取焦点，True，False|
|show_word_limit|boolean|否|是否显示输入字数统计，只在 type = "text" 或 type = "textarea" 时有效|
|slot|string|否|复合型输入框头部内容，只对 type="text" 有效，取值：prefix、suffix、prepend、append|
|slot_text|string|否|复合型输入框，相关位置显示的文本|
|style|string|否|原生属性，样式|

### 例子

```python

from django.db import models
from simplepro.components import fields


class CharModel(models.Model):
    """
    文本输入框，包含：input、password、textarea
    """

    # 这里的max_length是数据库字段的长度，也是界面上文本框可输入的长度
    # type属性 对应input原生的type

    # style 原生属性，可通过width设置宽度

    # 普通文本框
    f1 = fields.CharField(verbose_name='基础输入框', max_length=128, input_type='text', placeholder='单行输入',
                          autocomplete=False, style='width:100px;color:red;')

    # 多行文本框
    # 如果设置了style的高度，就不要设置 rows属性了，不然样式会乱掉
    f2 = fields.CharField(verbose_name='多行输入', max_length=128, input_type='textarea', show_word_limit=True,
                          placeholder='多行输入', clearable=False,
                          style='width:500px;', rows=20)
    # 密码输入框
    f3 = fields.CharField(verbose_name='密码', placeholder='请输入密码', max_length=128, show_password=True)

    f4 = fields.CharField(verbose_name='左边带图标', suffix_icon="el-icon-date", max_length=128)
    f5 = fields.CharField(verbose_name='右边带图标', prefix_icon="el-icon-search", max_length=128)

    f6 = fields.CharField(verbose_name='显示可输入长度', max_length=128, show_word_limit=True)

    # solt取值：prepend、append
    f7 = fields.CharField(verbose_name='复合输入框', max_length=128, slot='prepend', slot_text='https://', null=True,
                          blank=True)

    f8 = fields.CharField(verbose_name='复合输入框', max_length=128, slot='append', slot_text='.com', null=True, blank=True)

    class Meta:
        verbose_name = 'Char文本输入框'
        verbose_name_plural = 'Char文本输入框'

```

## ForeignKey 外键字段（Select）

ForeignKey 外键字段渲染成下拉框

可以自定义`queryset`和自定义远程搜索

> Django内置的ForeignKey 只支持全部加载数据和autocomplete，而SimplePro 支持自定义url搜索

### 效果
<img :src="$withBase('/images/foreignKey.png')" width='100%'>

### 字段
    
+ 类型

继承自`model.ForeignKey`字段

+ 包

`simplepro.components.fields.ForeignKey`

### 参数

除了下列参数以外，其他参数与`model.ForeignKey`一致。

|参数名|类型|必选|说明|
|---|---|---|---|
|disabled|boolean|否|是否禁用|
|size|string|否|输入框尺寸，medium/small/mini|
|clearable|boolean|否|是否可以清空选项，默认：True|
|placeholder|string|否|占位符|
|filterable|boolean|否|是否可搜索，默认：True|
|queryset|queryset对象或function|否|可以是一个model的queryset，也可以是一个方法，返回queryset|
|action|string|否|搜索url，远程搜索，filterable=True|
|limit|int|否|限制结果集大小，默认不限制|

### 关于queryset

如果使用了queryset 可能会遇到一个问题，就是输入框只显示id，而不显示label

这是因为 select的列表中不存在该选项

如果是在admin配置了`autocomplete_fields` 搜索将由django处理，`queryset`和`limit`无效。

### 关于搜索

搜索有3种形式，两种模式。

+ 模式1

列表过滤，直接Select进行过滤，返回符合条件的数据，不依赖网络接口。

+ 模式2

远程搜索，远程搜索分为内置`autocomplete_fields`和自定义url的远程搜索

`autocomplete_fields`是django内置支持的，需要在`ModelAdmin` 配置

自定义url的远程搜索，是自己在实现的相关业务逻辑，返回特定的参数

例子：

```python
def area_search(request):
    # 使用get 请求，参数名是：term ，是为了兼容 autocomplete_fields

    term = request.GET.get('term')

    rs = []
    areas = list(StudentArea.objects.filter(name__icontains=term))
    for item in areas:
        rs.append({
            'id': item.pk,
            'text': item.name
        })
    data = {
        'pagination': {
            'more': False
        },
        'results': rs
    }

    """
    返回 数据格式例子
    
    {
        pagination: {more: false},
        results:[
            {id: "1", text: "20级1班"},
            {id: "2", text: "20级2班"},
        ]
    }    
    """

    return HttpResponse(json.dumps(data), content_type='application/json')
```

字段配置：

```python

area = fields.ForeignKey(StudentArea, on_delete=models.SET_NULL, null=True, blank=True,
                         verbose_name='地区',
                         help_text='一对多', clearable=True, placeholder='选择地区',
                         # 指定自定义的url
                         action='/area/search'
                         )
```


### 完整例子

```python

from django.db import models
from simplepro.components import fields
# Select，和外键测试

# 这个demo可能有人要问，班级不是class吗？为什么要写成classes，因为class是关键字
class StudentClasses(models.Model):
    name = fields.CharField(max_length=32, verbose_name='班级名', show_word_limit=True)

    def __str__(self):
        return self.name


class StudentArea(models.Model):
    name = fields.CharField(max_length=32, verbose_name='地区', show_word_limit=True)

    def __str__(self):
        return self.name


class StudentOneToOneModel(models.Model):
    f = models.CharField(max_length=32, verbose_name='一对一')

    def __str__(self):
        return self.f


class StudentManyToManyModel(models.Model):
    f = models.CharField(max_length=32, verbose_name='多对多')

    def __str__(self):
        return self.f


# 外键字段可以设置 queryset 来进行数据的筛选
def get_student_class_queryset():
    return StudentClasses.objects.order_by('-pk')[:10]


class StudentModel(models.Model):
    name = fields.CharField(max_length=128, verbose_name='名字', default='张三')

    sex_choices = (
        (0, '男'),
        (1, '女'),
        (2, '未知'),
    )
    sex = fields.RadioField(verbose_name='性别', default=0, choices=sex_choices)

    star = fields.RateField(verbose_name='评价', default=5, help_text='给用户评级')

    money = fields.InputNumberField(verbose_name='存款', default=0)

    score = fields.SliderField(verbose_name='考试分数', default=100)

    # ForeignKey和OneToOneField、ManyToManyField 都支持两个参数
    # action 是指 select在搜索的时候 请求的url，后台只需要返回 一个数组就可以搜索数据了。[{'text':'张三','id':'123'}]
    # queryset 是指 select 默认展示数据的时候 框架会调用get_queryset 可以进行数据过滤这一类处理。

    # 外键字段 如果不指定action，可以在admin中配置：autocomplete_fields = ('classes',) 就可以自动搜索了。不配置两者 就只能列表过滤
    classes = fields.ForeignKey(StudentClasses, on_delete=models.SET_NULL, null=True, blank=True,
                                verbose_name='班级',
                                help_text='一对多', clearable=True, placeholder='选择班级',
                                queryset=get_student_class_queryset,
                                # 这里这里可以传入function，但是返回的必须是个queryset，也可以传入queryset
                                limit=100,# 这里限制默认显示的结果数量，设置下可以防止爆内存
                                )

    area = fields.ForeignKey(StudentArea, on_delete=models.SET_NULL, null=True, blank=True,
                             verbose_name='地区',
                             help_text='一对多', clearable=True, placeholder='选择地区',
                             # 指定自定义的url
                             action='/area/search'
                             )

    one_to_one = fields.OneToOneField(StudentOneToOneModel, on_delete=models.SET_NULL, null=True, blank=True,
                                      verbose_name='一对一字段')

    many_to_many = fields.ManyToManyField(StudentManyToManyModel, blank=True, verbose_name='多对多字段')
    # many_to_many = models.ManyToManyField(StudentManyToManyModel, blank=True, verbose_name='多对多字段')

    school_choices = (
        (0, '北大'),
        (1, '清华'),
        (2, '复旦'),
        (3, '交大'),
        (4, '厦大'),
        (5, '深大'),
        (6, '中山大学'),
        (7, '东南大学'),
        (8, '南开大学'),
    )
    school = models.IntegerField(verbose_name='学校', choices=school_choices, default=0)

    class Meta:
        verbose_name = 'Select下拉框'
        verbose_name_plural = 'Select下拉框'

    def __str__(self):
        return self.name

```


## OneToOneField 外键字段（Select）

一对一外键字段，渲染成select，除了字段命名不一致外，其他参数均与[ForeignKey 外键字段（Select）](#参数-12)

### 效果

<img :src="$withBase('/images/foreignKey.png')" width='100%'>

### 字段
    
+ 类型

继承自`model.OneToOneField`字段

+ 包

`simplepro.components.fields.OneToOneField`

### 参数

参考 [ForeignKey 外键字段 参数](#参数-12)

### 例子

参考 [ForeignKey 外键字段 例子](##完整例子)




## ManyToManyField 多对多字段 多选（Select）

多对多字段，渲染成select，除了字段命名不一致外，其他参数均与[ForeignKey 外键字段（Select）](#参数-12)

### 效果

<img :src="$withBase('/images/manytomany.png')" width='100%'>

### 字段
    
+ 类型

继承自`model.ManyToManyField`字段

+ 包

`simplepro.components.fields.ManyToManyField`

### 参数

参考 [ForeignKey 外键字段 参数](#参数-12)

### 例子

参考 [ForeignKey 外键字段 例子](##完整例子)




##  Transfer 穿梭框

穿梭框基于ManyToMany字段，可以进行多选。

### 效果

<img :src="$withBase('/images/transfer.png')" width='100%'>

### 字段
    
+ 类型

继承自`model.ManyToManyField`字段

+ 包

`simplepro.components.fields.TransferField`

### 参数

除了下列参数以外，其他参数与`model.ManyToManyField`一致。

|参数名|类型|必选|说明|
|---|---|---|---|
|filterable|boolean|否|	是否可搜索，默认：True|
|placeholder|string|否|搜索框占位符|
|titles|array|否|自定义列表标题，['列表 1', '列表 2']|
|button_texts|array|否|自定义按钮文案，['向左','向右']|
|format|string|否|列表顶部勾选状态文案，	{ noChecked: '${checked}/${total}', hasChecked: '${checked}/${total}' }|
|queryset|queryset或function|否|自定义查询|
|limit|int|否|限制默认结果大小|

### 例子

```python

from django.db import models
from simplepro.components import fields


class TransferModel(models.Model):
    name = fields.CharField(max_length=32, verbose_name='名字')

    transfer = fields.TransferField(TransferManyToManyModel, blank=True, verbose_name='穿梭框',
                                    help_text='基于many_to_many字段',
                                    filterable=True,  # 允许列表搜索
                                    placeholder='输入关键字搜索',  # 搜索框占位符
                                    titles=['待选', '已选'],  # 自定义穿梭框title
                                    button_texts=['往左', '往右']  # 自定义按钮文本
                                    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Transfer 穿梭框'
        verbose_name_plural = 'Transfer 穿梭框'

```

##  高德地图

字段可以使用高德地图选取坐标和地址

### 效果

<img :src="$withBase('/images/amap.png')" width='100%'>

### 字段
    
+ 类型

继承自`model.CharField`字段

+ 包

`simplepro.components.fields.AMapField`

### 参数

除了下列参数以外，其他参数与`model.CharField`一致。

|参数名|类型|必选|说明|
|---|---|---|---|
|api_key|string|否|	配置高德地图apikey，需要自行去高德官网申请，如果不填写，将使用simplepro官方申请的默认的，后续如果我们删除了这个key，会造成业务问题。|
|width|string|否|地图宽度，默认500px|
|height|string|否|地图高度，默认300px|
|style|string|否|html中的样式属性，可以设置边框、背景等等，例如：`border:#ccc 1px solid;backgroud:#fff;`|
|pick_type|string|否|坐标拾取类型，`geo`返回逗号分隔的经纬度，`address`返回地址文本，默认为`geo`|

### 例子

```python

from django.db import models
from simplepro.components import fields


class AMapModel(models.Model):
    name = fields.CharField(verbose_name='名称', show_word_limit=True, null=True, blank=True, max_length=64)
    geo = fields.AMapField(max_length=32, verbose_name='经纬度', null=True, blank=True, help_text='点击地图获取经纬度')

    # pick_type 取值为 geo、address
    # geo 获取经纬度
    # address 获取地址
    address = fields.AMapField(max_length=128, verbose_name='地址', null=True, blank=True, help_text='点击地图获取地址',
                               pick_type='address')

    def __str__(self):
        return str(self.name)

    class Meta:
        verbose_name = '高德地图组件'
        verbose_name_plural = '高德地图组件'

```



##  视频播放器

可以再列表页和编辑页 播放视频

### 效果

<img :src="$withBase('/images/video.png')" width='100%'>

### 字段
    
+ 类型

继承自`model.CharField`字段

+ 包

`simplepro.components.fields.VideoField`

### 参数

参数与`model.CharField`一致。

### 例子

```python

from django.db import models
from simplepro.components import fields

# 从simplepro 5.0.0版本开始，支持视频播放组件
class VideoModel(models.Model):
    name = fields.CharField(verbose_name='名称', show_word_limit=True, null=True, blank=True, max_length=64)

    video = fields.VideoField(max_length=128, verbose_name='视频播放', null=True, blank=True, help_text='视频播放组件')

    def __str__(self):
        return str(self.name)

    class Meta:
        verbose_name = '视频播放组件'
        verbose_name_plural = '视频播放组件'

```



##  树形下拉框

可以在列表页和编辑页 使用

### 效果

<img :src="$withBase('/images/tree_combobox.png')" width='100%'>

### 字段
    
+ 类型

继承自`model.ForeignKey`字段

+ 包

`simplepro.components.fields.TreeComboboxField`

### 参数

参数与`model.ForeignKey`一致。

|参数名|类型|必选|说明|
|---|---|---|---|
|queryset|QuerySet或function|否|自定义数据查询对象，可用于过滤下拉框的一些数据，比如软删除之类的需求|
|strictly|boolean|否|严格模式，指定树形选择器是否可以选择任意单个节点，还是只能选择最后一个子节点，默认为`False`，只能选择最后一个节点|

### 错误提示

如果出现以下错误，请检查字段传入的queryset参数，是否是一个function(返回的也要是Queryset)或者是一个QuerySet对象，如果不是就会出现错误。

```python

raise ValueError('queryset must be a QuerySet or a function')
```

### 例子

```python

class TreeComboboxModel(models.Model):
    name = fields.CharField(max_length=32, verbose_name='名字')
    # 我们需要再model中加入simplepro的TreeCombobox组件
    parent = fields.TreeComboboxField('self', on_delete=models.CASCADE, null=True, blank=True, verbose_name='父级',
                                      strictly=True,  # 是否严格模式，严格模式只能选择叶子节点
                                      # 通过get_queryset方法获取数据
                                      queryset=_get_combobox_queryset,
                                      help_text="树形下拉框，选择父级")

    def __str__(self):
        return str(self.name)

    class Meta:
        verbose_name = '树形下拉框'
        verbose_name_plural = '树形下拉框'
```


### 视频演示

<iframe src="//player.bilibili.com/player.html?aid=944593753&bvid=BV16W4y1j7nm&cid=875095279&page=1" scrolling="no" border="0" frameborder="no" framespacing="0" allowfullscreen="true"> </iframe>