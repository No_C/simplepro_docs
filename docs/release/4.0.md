# 4.0

> 发布日期 2022 年 04 月 01 日

## 新增功能

- 支持`ModalDialog`,[文档](/dialog.html)
- 支持`MultipleCellDialog`，[文档](/dialog.html)
- 支持`Layer`使用所有`element-ui`的属性， [文档](/layer.html#extras-字段)
- `ModelAdmin`和`Model`的自定义列，支持使用`Vue`组件或者`element-ui`组件，[文档](/table.html#自定义列)

- 列表页新增两个方法，用于`ModalDialog`控制窗体关闭和刷新列表数据，[文档](/dialog.html#自定义页面操作列表)

## 其他

优化了大量的逻辑代码，提升了性能。


如果以下两个报错，请删除虚拟环境，或者删除项目根目录下的所有`.pyc`文件即可。

```
ImportError: cannot import name 'MultipleCellDialog' from 'simplepro.dialog' 
ImportError: cannot import name 'ModalDialog' from 'simplepro.dialog' 
```