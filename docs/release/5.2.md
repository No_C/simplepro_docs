# 5.2

发布时间：2022-08-01

## 修复

+ 列表数据为`数值0`的时候，无法显示的bug
+ 修复没有权限的时候，还能访问页面的bug
+ 修复权限显示bug

## 增加

+ 增加未授权提示页面
+ 命令添加自定义菜单的权限
+ 首页布局增加自适应适配


## 命令行

我们在该版本中增加了命令行，用以支持自定义菜单的权限和激活SimplePro

[点击查看参考文档](./command.md)

+ 执行自定义菜单权限添加

```shell
python manage.py migrate_menu
```
+ 执行pro激活

```shell
python manage.py active 123456
```
