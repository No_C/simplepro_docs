# simplepro 自定义权限

+ 在simpleui中没有提供自定义权限的支持。
+ 在原生django admin中没有提供自定义菜单权限的支持，自定义按钮权限支持。

## 提示

SimplePro使用的自定义权限，也只是向Django添加数据，具体的权限相关均由Django提供支持，如遇权限相关问题请结合本文档+Django文档。

## 权限基础知识

simplepro的权限 也是在django admin的权限中进行扩展的。但是未更改django 权限相关的任何字段。

### 表结构

|表名|名字|作用|
|---|---|---|
|django_content_type|模块类型表|用于记录用户和admin的各个模块|
|auth_permission|权限表|用户记录所有模块的view,add,change,delete权限，与django_content_type关联|
|auth_group|权限组|每个用户都可以指定一个或多个权限组，与auth_group_permissions关联|

以上三个表就是权限的基础表结构，通过表格或者`model`的`permission`字段所添加的权限，都会往`auth_permission`表中存入数据。



## 自定义按钮权限

自定义权限包括自定义按钮和自定义菜单，django admin中 对自定义的action 也就是自定义按钮提供了权限支持，定义如下：


> 注意：下面这种方式在simplepro中和原生admin中通用，simpleui中不适用。

### 1.定义

```python

# admin
@admin.register(Employe)
class EmployeAdmin(admin.ModelAdmin):
    #....定义显示字段之类

    actions=[test]

    # 自定义按钮
    def test(self, request, queryset):
        # 这里可有做一些crud
        pass

    test.short_descript='按钮显示的名'    

# model
class Employe(models.Model):
    name = models.CharField(max_length=128, verbose_name='名称', help_text='员工的名字', null=False, blank=False,
                            db_index=True)

    gender_choices = (
        (0, '未知'),
        (1, '男'),
        (2, '女'),
    )

    gender = models.IntegerField(choices=gender_choices, verbose_name='性别', default=0)

    class Meta:
        verbose_name = "员工"
        verbose_name_plural = "员工管理"

        # 定义自定义的权限
        permissions = (
            ('test', 'test111'),
            ('test2', 'test222'),
            ('codename','权限显示的名'), # 注意 这里的codename要与admin中的按钮方法名一致
            
        )`

```

### 2.迁移

+ 生成迁移文件

```shell
python3 manage.py makemigrations
```
+ 执行迁移，添加到数据库

```shell
 python3 manage.py migrate
```

### 3.勾选权限
在操作完以上两个步骤后，我们可以进入到`认证和授权->组`或者`认证和授权->权限`中，对用户勾选该项权限。

### 4.其他操作

在`认证和授权->权限`该项菜单中，可以直接用表格形式添加。可以不用写在model中，但是这种方式容易遗漏，不推荐使用。


## 自定义菜单权限

自定义菜单，必须要有`codename`，如果是多级的情况，父级必须要有`codename`否则权限将会不正常。所以建议是全部自定义菜单都加上`codename`

如果没有`codename` 将会不受权限限制，所有用户都可以显示。 另外超级用户也是不受权限的控制。

`5.2`及以上版本与低版本存在一些不兼容，请以本文档为准。


### 命令行

为了降低自定义菜单的的复杂度，我们从5.2版本中推出了命令行的方式。

增加了命令行方式对自定义菜单进行权限自动添加，自定义菜单[参考文档](./config.md)

> SimplePro 5.2 及以上可用

```shell
python manage.py migrate_menu
```

### 菜单示例

```python

SIMPLEUI_CONFIG = {
    # 在自定义菜单的基础上保留系统模块
    'system_keep': True,
    'dynamic': False,
    'menus': [{
        'name': '社区',
        'icon': 'fas fa-code',
        'url': 'https://simpleui.72wo.com',
        'codename': 'community'
    }, {
        'name': '产品',
        'icon': 'fa fa-file',
        'codename': 'product',
        'models': [{
            'name': 'SimplePro',
            'codename': 'SimplePro',
            'icon': 'far fa-surprise',
            'models': [{
                'name': 'Pro文档',
                'url': 'https://simpleui.72wo.com/docs/simplepro'
            }, {
                'name': '购买Pro',
                'url': 'http://simpleui.72wo.com/simplepro'
            }]
        }, {
            'name': 'SimpleUI',
            'url': 'https://github.com/newpanjing/simpleui',
            'icon': 'fab fa-github',
            'codename': 'simpleui',
            'newTab': True
        }, {
            'name': '图片转换器',
            'url': 'https://convert.72wo.com',
            'icon': 'fab fa-github',
            'codename': 'convert',
            'newTab': True
        }, {
            'name': '全文检索',
            'url': 'https://github.com/sea-team/gofound',
            'icon': 'fab fa-github',
            'codename': 'gofound',
            'newTab': True
        }]
    }]
}
```

## 不生效排查

1. 请保证 `request.user.get_all_permissions()` 该方法中有自定义的权限
2. 顶级菜单的权限是否存在，例如test.test 如果该项权限没有，子级也不会显示
3. 由于缓存问题，需要重启服务

## 超级用户和非超级用户的区别

如果增加了action权限，非超级用户 没有勾选该项权限 将无法显示。

如果模块有重复，将会产生bug

判断是否重复的依据：

```python

Permission.objects.filter(codename=codename, content_type__app_label=admin.opts.app_label,
                                                   content_type__model=admin.opts.model_name).exists()

```