# 全局配置


## 首页配置
simplepro首页默认了较多的模块，也许某些你无法用上。所以该文档提供了如何定制。

simpleui采用框架结构，simplepro也是基于simpleui，simpleui的首页文件`home.html`

只要重写该页面即可。

重写有两种方式，第一种全覆盖，第二种block方式

### 模板重写
#### 全覆盖方式
在你的项目的templates目录中建立以下文件结构
```
├─templates
│  ├─admin
│  │  ├─home.html
```
home.html内容：
```html
{% load i18n static simpletags %}
<link rel="stylesheet" href="{% static 'admin/simplepro/css/home.css' %}">
<script type="text/javascript" src="{% static 'admin/simplepro/echarts/echarts.min.js' %}"></script>
<div class="home-body">

     {% block quick %}
        {% include 'admin/parts/quick.html' %}
    {% endblock %}

    {% block chart_cards %}
        {% include 'admin/parts/charts_cards.html' %}
    {% endblock %}

    {% block line_chart %}
       {% include 'admin/parts/line_chart.html' %}
    {% endblock %}

    {% block info %}
       {% include 'admin/parts/info.html' %}
    {% endblock %}


    <div style="height: 80px"></div>
</div>
<el-backtop target=".home-body"></el-backtop>
```
你可以完全拷贝内容，到新建的home.html中，挑选需要的include部分。

#### 继承block方式
也需要在templates->admin->home.html中建立文件
html内容：

```html
{% extends 'admin/home.html' %}
{% block quick %}
    //...这里重新你的内容
{% endblock%}

```
block方式文档请参考django的文档。

### 配置方式

simplepro内置了一个默认的首页，只是由简单的快捷导航与最近操作组成。

在自己项目的`settings.py`中加入：

### 配置URL
```python
SIMPLEUI_HOME_PAGE = 'https://www.baidu.com'
```

### 标题

```python
SIMPLEUI_HOME_TITLE = '百度一下你就知道'
```

### 图标

支持element-ui和fontawesome的图标，参考https://fontawesome.com/icons图标

```python
SIMPLEUI_HOME_ICON = 'fa fa-user'
```

> 三项配置都是选填，不填都会有默认值。 图标列表


### 首页监控图表显示和隐藏

> 默认显示，自3.1 起生效

```python
# 显示
SIMPLEPRO_MONIT_DISPLAY = True

# 隐藏
SIMPLEPRO_MONIT_DISPLAY = False
```

## 隐藏 simplepro 版本和授权信息

请在settings.py中加入：
```python
SIMPLEPRO_INFO = False
```

## 站点标题

> 显示在浏览器标签页和系统相关位置的名称

可以在`urls.py`中加入：
```python
from django.contrib import admin
# 网站标签页名称
admin.site.site_title = '管理后台PRO'

# 网站名称：显示在登录页和首页
admin.site.site_header = '员工管理后台PRO'
```

## 收藏夹\网页小图标

+ 将下载好的图标放入到项目的`static`目录中，然后取名为`favicon.ico`

+ 然后在`urls.py`中加入：

```python
urlpatterns = [
    # 这里可以配置网页收藏夹的图标
    path('favicon.ico', RedirectView.as_view(url=r'static/favicon.ico')),
] 
```

## 登录页logo

在`settings.py`中加入：


+ 绝对路径

```python
SIMPLEUI_LOGO='https://mat1.gtimg.com/pingjs/ext2020/qqindex2018/dist/img/qq_logo_2x.png'
```

+ 相对路径
> 需要将图片放入`static`目录中，然后引用相关路径
```python
SIMPLEUI_LOGO='static/images/logo.png'
```

+ 效果

登录页：

<img :src="$withBase('/images/logo1.png')" width='100%'>

首页： 
<img :src="$withBase('/images/logo2.png')" width='100%'>

## 首页logo

首页的logo暂时没有单独的配置，与登录页的logo一样。

## 首页和登录页不同logo配置

大部分业务不能满足以上两种配置，这时候可以很灵活的用模板重写来实现。simple系列框架预留了大量的block，可以很轻松的进行重写。

### 重写登录页logo

> 我们推荐用这种方式重写，而不推荐覆盖整个文件，或者直接修改依赖包中的模板。下列这种方式不会影响后续的升级。

+ 在项目中创建文件

`templates/admin/login.html`

+ 文件中加入

src可以写相对路径或者绝对路径
```html

{% block logo %}
    <div class="banner">
        <img src="{% static 'admin/simplepro/images/banner.png' %}">
    </div>
{% endblock %}

```

### 重写首页logo

+ 在项目中创建文件

`templates/admin/index.html`

+ 文件中加入

src可以写相对路径或者绝对路径，site_header可以写网站名称
```html

{% block logo %}
    <div class="logo-wrap" v-if="!fold">
        <div class="float-wrap">
            <div class="left">
                <img src="{% static '/admin/simplepro/images/logo.png' %}">
            </div>
            <div class="left">
                <span>{{ site_header }}</span>
            </div>
        </div>
    </div>
{% endblock %}

```

## 离线模式

> 在3.3.6以及以后版本中，离线模式已经正式取消，该属性将会被忽略。

离线模式是指以脱机模式加载相关静态资源，适合没有外网的场景。可以配合离线激活文件使用。


在`settings.py`中加入

```python
SIMPLEUI_STATIC_OFFLINE = True
```

## 图标

### 说明
simpleui中显示的图标 可以参考[fontawesome](https://fontawesome.com/icons?d=gallery)的图标，只需要将完整的class名填入即可。


### 默认图标
simpleui对所有菜单提供了一个默认的file图标，是为了统一风格。也许你并不喜欢，你可以选择关闭默认图标

>SIMPLEUI_DEFAULT_ICON = False

|值|说明|
|--|--|
|True|开启默认图标，默认为True|
|False|关闭默认图标|

### 自定义图标
simpleui仅为系统默认模块提供了图标，如果要为其他模块指定图标，可以自定义配置。图标参考请查阅：[图标说明](#图标说明)

优先级：
自定义->系统配图->默认图标

>注：simpleui 原则上不涉及代码，所以采用setting方式。后续可考虑扩展Model的 Meta class 进行配置图标

|字段|说明|
|---|---|
|name|模块名字，请注意不是model的命名，而是菜单栏上显示的文本，因为model是可以重复的，会导致无法区分|
|icon|图标|
例子：
```
SIMPLEUI_ICON = {
    '系统管理': 'fab fa-apple',
    '员工管理': 'fas fa-user-tie'
}

```



## 菜单

### 自定义菜单

### system_keep 保留系统菜单
该字段用于告诉simpleui，是否需要保留系统默认的菜单，默认为False，不保留。
如果改为True，自定义和系统菜单将会并存

### menu_display 过滤显示菜单和排序功能
该字段用于告诉simpleui，是否需要开启过滤显示菜单和排序功能。<br>
默认可以不用填写，缺省配置为默认排序，不对菜单进行过滤和排序。<br>
开启认为传一个列表，如果列表为空，则什么也不显示。列表中的每个元素要对应到menus里面的name字段

### dynamic 开启动态菜单功能
该字段用于告诉simpleui，是否需要开启动态菜单功能。<br>
默认可以不用填写，缺省配置为False，不开启动态菜单功能。<br>
开启为True，开启后，每次用户登陆都会刷新左侧菜单配置。<br>
需要注意的是：开启后每次访问admin都会重读配置文件，所以会带来额外的消耗。

### menus说明  (支持3级菜单)

|字段|说明|
|---|---|
|name|菜单名|
|icon|图标，参考element-ui和fontawesome图标|
|url|链接地址，绝对或者相对,如果存在models字段，将忽略url|
|models|子菜单，自`simpleui` 2021.02.01+版本 支持最多3级菜单，使用方法可以看下方例子|
|newTab|浏览器新标签打开|
|codename|权限标识，需要唯一|

### 例子

```python
import time
SIMPLEUI_CONFIG = {
    # 在自定义菜单的基础上保留系统模块
    'system_keep': True,
    'dynamic': False,
    'menus': [{
        'name': '社区',
        'icon': 'fas fa-code',
        'url': 'https://simpleui.72wo.com',
        'codename': 'community'
    }, {
        'name': '产品',
        'icon': 'fa fa-file',
        'codename': 'product',
        'models': [{
            'name': 'SimplePro',
            'codename': 'SimplePro',
            'icon': 'far fa-surprise',
            'models': [{
                'name': 'Pro文档',
                'url': 'https://simpleui.72wo.com/docs/simplepro'
            }, {
                'name': '购买Pro',
                'url': 'http://simpleui.72wo.com/simplepro'
            }]
        }, {
            'name': 'SimpleUI',
            'url': 'https://github.com/newpanjing/simpleui',
            'icon': 'fab fa-github',
            'codename': 'simpleui',
            'newTab': True
        }, {
            'name': '图片转换器',
            'url': 'https://convert.72wo.com',
            'icon': 'fab fa-github',
            'codename': 'convert',
            'newTab': True
        }, {
            'name': '全文检索',
            'url': 'https://github.com/sea-team/gofound',
            'icon': 'fab fa-github',
            'codename': 'gofound',
            'newTab': True
        }]
    }]
}
```
如果SIMPLEUI_CONFIG中存在menus字段，将会覆盖系统默认菜单。并且menus中输出的菜单不会受权限控制。
需要权限控制参考[自定义菜单权限](./permissions.md)

## SIMPLEPRO_FK_ASYNC_DATA

+ 在6.3+的版本中，SimplePro组件中的外键字段（ForeignKey、ManyToManyField、TransferField）新增了全局属性 `SIMPLEPRO_FK_ASYNC_DATA`用于配置是否默认用ajax获取数据，默认为`Flase`

该设置由于是初期可能存在部份兼容问题，默认关闭，如果追求性能可以开启该配置。

```python
# 指定SimplePro以异步的方式获取外键数据，自6.3+ 开始支持
SIMPLEPRO_FK_ASYNC_DATA = True
```

